import argparse
import os


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("-mp", "--model-path", type=str, default="./models/model.pth")
args = arg_parser.parse_args()

if not os.path.isdir("./bert-quote-attribution"):
    os.system(
        "git clone https://gitlab.com/Aethor/bert-quote-attribution bert_quote_attribution"
    )

from bert_quote_attribution.main import load_model

model = load_model()
