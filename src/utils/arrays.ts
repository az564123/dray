export function arrayEquals(a1: Array<any>, a2: Array<any>): boolean {
    if (a1.length !== a2.length) {
	return false;
    }
    for (let i = 0; i < a1.length; i++){
	if (a1[i] !== a2[i]) {
	    return false;
	}
    }
    return true;
}
