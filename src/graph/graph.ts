import * as d3 from 'd3'
import * as fs from 'fs'
import $ = require('jquery')
import { Analyse, AnalyseCharacter, Interaction } from '../analyseTypes'
import { Filter, allFilterTypes } from './filters'
import { Lens, allLenseTypes } from './lenses'
import { ipcRenderer, remote } from 'electron'

export type Node = {
  id: string
  interactionsCount: number
  radius: number
  x?: number
  y?: number
}

export type Link = {
  target: Node
  source: Node
  strength: number
}

let nodes: Node[] = []
let links: Link[] = []

let filters: Filter[] = []
let lens: Lens | undefined = undefined

let interactions: Interaction[] | undefined = undefined
let curInteractionIdx: number | undefined = undefined

const maxNodeRadius = 50
const minNodeRadius = 5
const maxLinkSize = 2
const minLinkSize = 1
let width = 800
let height = 600
let maxInteractionsCount = 0

function viewportForce(alpha: number) {
  for (let node of nodes) {
    node.x = Math.min(Math.max(node.x, 2 * node.radius), width - 2 * node.radius)
    node.y = Math.min(Math.max(node.y, 2 * node.radius), height - 2 * node.radius)
  }
}
const simulation = d3
  .forceSimulation(nodes)
  .force('center', d3.forceCenter(width / 2, height / 2))
  .force(
    'charge',
    d3.forceManyBody().strength((d: Node) => -50 - d.interactionsCount)
  )
  .force(
    'link',
    d3.forceLink(links).strength((d: Link) => {
      return d.strength
    })
  )
  .force(
    'collision',
    d3.forceCollide().radius((d: Node) => {
      return d.radius
    })
  )
  .force('viewport', viewportForce)

function initUI(simulation: d3.Simulation<Node, undefined>) {
  $('#bottom-panel-ui').load('graph/bottomPanelUI.html', () => {
    let pauseButton = document.getElementById('pause-button')
    pauseButton.textContent = '⏸'
    let isStopped = false
    pauseButton.addEventListener('click', (event) => {
      if (isStopped) {
        simulation.restart()
        isStopped = false
        pauseButton.textContent = '⏸'
      } else {
        simulation.stop()
        isStopped = true
        pauseButton.textContent = '▶'
      }
    })

    let progressRange = document.getElementById('progress-range')
    progressRange.addEventListener('input', (event: any) => {
      if (!interactions || !curInteractionIdx) {
        return
      }
      let newInteractionIdx = Math.floor((event.target.value / 100) * interactions.length)
      if (newInteractionIdx < curInteractionIdx) {
        for (let i = curInteractionIdx - 1; i >= newInteractionIdx; i -= 1) {
          popInteraction(interactions[i])
        }
      } else {
        for (let i = curInteractionIdx; i < newInteractionIdx; i += 1) {
          pushInteraction(interactions[i])
        }
      }
      curInteractionIdx = newInteractionIdx
      draw()
    })
  })

  $('#left-panel-ui').load('graph/leftPanelUI.html', () => {
    let filterAddList = document.createElement('select')
    $('#filters-ui-add').append(filterAddList)
    for (let filterType of allFilterTypes) {
      let option = document.createElement('option')
      option.textContent = filterType.name
      option.setAttribute('value', filterType.name)
      filterAddList.appendChild(option)
    }
    let filterAddButton = document.createElement('button')
    filterAddButton.textContent = '+'
    filterAddButton.addEventListener('click', (event) => {
      let filterType = allFilterTypes.find((filterType) => {
        return filterType.name === filterAddList.options[filterAddList.selectedIndex].text
      })
      if (!filterType) {
        return
      }

      let filter = new filterType()
      filters.push(filter)
      draw()
      draw()

      let filterLabel = document.createElement('label')
      filterLabel.setAttribute('class', 'hover-label')
      filterLabel.addEventListener('click', (event) => {
        let rightPanel = document.getElementById('right-panel-ui')
        rightPanel.innerHTML = ''
        filter.buildUI(rightPanel, draw)
      })
      filterLabel.textContent = filterType.name

      let filterDelButton = document.createElement('button')
      filterDelButton.addEventListener('click', (event) => {
        filters = filters.filter((el) => {
          return el !== filter
        })
        draw()
        draw()
        filterLabel.remove()
        filterDelButton.remove()
      })
      filterDelButton.textContent = '\u274c'

      $('#filters-ui-filters').append(filterLabel)
      $('#filters-ui-filters').append(filterDelButton)
    })
    $('#filters-ui-add').append(filterAddButton)

    let lensesDiv = document.getElementById('lenses-ui')
    for (let lensType of allLenseTypes) {
      let lensButton = document.createElement('input')
      lensButton.setAttribute('type', 'radio')
      lensButton.setAttribute('name', 'lenses')
      lensButton.setAttribute('id', lensType.name)
      lensButton.addEventListener('click', (event) => {
        lens = new lensType()
        draw()
      })

      let lensLabel = document.createElement('label')
      lensLabel.setAttribute('class', 'hover-label')
      lensLabel.textContent = lensType.name
      lensLabel.addEventListener('click', (e) => {
        let rightPanel = document.getElementById('right-panel-ui')
        rightPanel.innerHTML = ''
        lens.buildUI(rightPanel, draw)
      })

      lensesDiv.appendChild(lensLabel)
      lensesDiv.appendChild(lensButton)
    }
  })
}

function igniteSimulation() {
  simulation.nodes(nodes)
  simulation.force('link', d3.forceLink(links))
  simulation.alpha(1)
}

function drawNodes(localNodes: Node[] | undefined, localLinks: Link[] | undefined): d3.Selection<d3.BaseType, Node, d3.BaseType, {}> {
  if (!localNodes) {
    localNodes = nodes
  }
  if (!localLinks) {
    localLinks = links
  }

  let camera = d3.select('#camera')

  let nodeSel = camera.selectAll('.node').data(localNodes, (d: Node) => {
    return d.id
  })

  // enter
  nodeSel
    .enter()
    .append('g')
    .attr('class', 'node')
    .call((parent) => {
      parent.append('circle').attr('r', (d: Node) => {
        return d.radius
      })
      parent.append('text').text((d: Node) => {
        return d.id
      })
    })

  //exit
  nodeSel.exit().remove()

  // update
  nodeSel
    .attr('transform', (d: Node) => {
      return 'translate(' + d.x + ',' + d.y + ')'
    })
    .call((parent) => {
      parent
        .selectAll('circle')
        .attr('r', (d: Node) => {
          return d.radius
        })
        .attr('fill', '#687')
      parent.selectAll('text').text((d: Node) => {
        return d.id
      })
    })

  return nodeSel
}

function drawLinks(localNodes: Node[] | undefined, localLinks: Link[] | undefined): d3.Selection<d3.BaseType, Link, d3.BaseType, {}> {
  if (!localNodes) {
    localNodes = nodes
  }
  if (!localLinks) {
    localLinks = links
  }

  let camera = d3.select('#camera')

  // update
  const linkSel = camera
    .selectAll('line')
    .data(localLinks)
    .attr('stroke-width', (d: Link) => {
      return Math.min(maxLinkSize, Math.max(minLinkSize, d.strength))
    })
    .attr('stroke', (d: Link) => {
      return d.strength > 0 ? '#00cc00' : '#cc0000'
    })
    .attr('x1', (d: Link) => {
      return d.source.x
    })
    .attr('x2', (d: Link) => {
      return d.target.x
    })
    .attr('y1', (d: Link) => {
      return d.source.y
    })
    .attr('y2', (d: Link) => {
      return d.target.y
    })

  // enter
  linkSel
    .enter()
    .append('line')
    .attr('stroke-opacity', 0.8)
    .attr('stroke', (d: Link) => {
      return d.strength > 0 ? '#00cc00' : '#cc0000'
    })
    .attr('stroke-width', (d: Link) => {
      return Math.min(maxLinkSize, Math.max(minLinkSize, d.strength))
    })

  // exit
  linkSel.exit().remove()

  return linkSel
}

function pushInteraction(interaction: Interaction | undefined) {
  if (!interaction) {
    console.warn('tried to push undefined interaction')
    return
  }

  let activeNode = nodes.find((node) => {
    return node.id === interaction.activeCharacter
  })

  if (activeNode) {
    activeNode.interactionsCount += 1
    activeNode.radius = minNodeRadius + ((maxNodeRadius - minNodeRadius) * activeNode.interactionsCount) / maxInteractionsCount
  } else {
    activeNode = {
      id: interaction.activeCharacter,
      interactionsCount: 1,
      radius: minNodeRadius,
      x: width / 2,
      y: height / 2,
    }
    nodes.push(activeNode)
  }

  for (let targetCharacter of interaction.targetCharacters) {
    let targetNode = nodes.find((node) => {
      return node.id === targetCharacter.name
    })
    if (targetNode) {
      targetNode.interactionsCount += 1
      targetNode.radius = minNodeRadius + ((maxNodeRadius - minNodeRadius) * targetNode.interactionsCount) / maxInteractionsCount
      continue
    }
    nodes.push({
      id: targetCharacter.name,
      interactionsCount: 1,
      radius: minNodeRadius,
      x: activeNode.x,
      y: activeNode.y,
    })
  }

  // Add links if necesseary
  for (let targetCharacter of interaction.targetCharacters) {
    let found = false
    for (let link of links) {
      if (link.source.id !== interaction.activeCharacter) {
        continue
      }
      if (link.target.id !== targetCharacter.name) {
        continue
      }
      found = true
      link.strength += targetCharacter.influence
      break
    }
    if (!found) {
      links.push({
        source: nodes.find((node: Node) => {
          return node.id === interaction.activeCharacter
        }),
        target: nodes.find((node: Node) => {
          return node.id === targetCharacter.name
        }),
        strength: targetCharacter.influence,
      })
    }
  }

  curInteractionIdx += 1
}

function popInteraction(interaction: Interaction | undefined) {
  if (!interaction) {
    console.warn('tried to pop undefined interaction')
    return
  }

  let nodesToRemove = []

  let activeNode = nodes.find((node) => {
    return node.id === interaction.activeCharacter
  })
  if (!activeNode) {
    console.warn('tried to pop interaction, but ' + interaction.activeCharacter + ' was not found in the graph')
    return
  }
  activeNode.interactionsCount -= 1
  if (activeNode.interactionsCount == 0) {
    nodesToRemove.push(activeNode)
  } else {
    activeNode.radius = minNodeRadius + ((maxNodeRadius - minNodeRadius) * activeNode.interactionsCount) / maxInteractionsCount
  }

  for (let targetCharacter of interaction.targetCharacters) {
    let targetNode = nodes.find((node) => {
      return node.id === targetCharacter.name
    })
    if (targetNode) {
      targetNode.interactionsCount -= 1
      if (targetNode.interactionsCount == 0) {
        nodesToRemove.push(targetNode)
      } else if (targetNode.interactionsCount > 0) {
        targetNode.radius = minNodeRadius + ((maxNodeRadius - minNodeRadius) * targetNode.interactionsCount) / maxInteractionsCount
      }
    }
  }

  for (let node of nodesToRemove) {
    nodes = nodes.filter((n) => {
      return n.id !== node.id
    })
  }
  for (let node of nodesToRemove) {
    for (let link of links) {
      links = links.filter((l) => {
        return l.source.id !== node.id && l.target.id !== node.id
      })
    }
  }

  curInteractionIdx -= 1
}

function draw() {
  let filteredNodes = nodes
  let filteredLinks = links
  for (let filter of filters) {
    ;({ nodes: filteredNodes, links: filteredLinks } = filter.call(nodes, links))
  }

  let nodesSelection = drawNodes(filteredNodes, filteredLinks)
  let linksSelection = drawLinks(filteredNodes, filteredLinks)

  if (lens) {
    lens.call(nodesSelection, linksSelection)
  }
}

export function startSimulation(analyse: Analyse) {
  maxInteractionsCount = analyse.interactions.length

  initUI(simulation)

  let svg = d3.select('#canvas').append('svg')

  function setSVGCanvasDimension() {
    let canvas = document.getElementById('canvas')
    width = canvas.offsetWidth
    height = canvas.offsetHeight
    svg.attr('width', width)
    svg.attr('height', height)
  }
  setSVGCanvasDimension()
  window.addEventListener('resize', () => {
    setSVGCanvasDimension()
  })

  ipcRenderer.send('set-contextual-menu', [
    {
      label: 'Graph...',
      submenu: [
        {
          label: 'Save as svg...',
          signal: 'graph-save-svg',
        },
      ],
    },
  ])
  ipcRenderer.on('graph-save-svg', () => {
    remote.dialog
      .showSaveDialog({ title: 'Save graph...' })
      .then((dialogResult: Electron.SaveDialogReturnValue) => {
        if (dialogResult.canceled) {
          return
        }
        fs.writeFile(dialogResult.filePath, document.getElementById('canvas').innerHTML, (err) => {
          if (err) {
            console.log(err)
            return
          }
        })
      })
      .catch()
  })

  let camera = svg.append('g').attr('id', 'camera')

  svg.call(
    d3.zoom().on('zoom', () => {
      camera.attr('transform', 'translate(' + d3.event.transform.x + ',' + d3.event.transform.y + ')' + 'scale(' + d3.event.transform.k + ')')
    })
  )

  interactions = analyse.interactions
  curInteractionIdx = 0

  simulation.on('tick', () => {
    const curInteraction: Interaction = interactions[curInteractionIdx]
    if (curInteraction) {
      pushInteraction(curInteraction)
      igniteSimulation()
    }

    draw()

    let progressRange = document.getElementById('progress-range') as HTMLInputElement
    if (progressRange) {
      progressRange.value = ((curInteractionIdx * 100) / analyse.interactions.length).toString()
    }
  })

  simulation.alpha(1).restart()
}
