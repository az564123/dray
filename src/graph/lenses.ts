import { Node, Link } from './graph'
import { arrayEquals } from '../utils/arrays'
import { colorsList } from '../utils/colors'
import * as d3 from 'd3';


type d3NodesSelection = d3.Selection<d3.BaseType, Node, d3.BaseType, {}>;
type d3LinksSelection = d3.Selection<d3.BaseType, Link, d3.BaseType, {}>;

export interface Lens {
    call: (nodesSelection: d3NodesSelection, linksSelection: d3LinksSelection) => void,
    buildUI: (parent: HTMLElement, redrawCallback: () => void) => void
};

export class DegreeCentralityLens implements Lens {

    k: number;

    constructor(k: number = 1) {
	if (k < 0) {
	    throw new Error(`invalid value of k : ${k}`);
	}
	this.k = k
    }

    call(nodesSelection: d3NodesSelection, linksSelection: d3LinksSelection) {
	let nodes = nodesSelection.data();
	nodes.sort((node1: Node, node2: Node) => {
	    return node2.interactionsCount - node1.interactionsCount;
	});
	let centralNodes = new Set(nodes.slice(0, this.k));

	nodesSelection.selectAll('circle')
	    .filter((node: Node) => {
		return centralNodes.has(node)
	    })
	    .attr('fill', colorsList[0]);
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	
    }
}

export class ClusterKMeansLens implements Lens {

    k: number;

    constructor(k: number = 5) {
	if (k <= 0) {
	    throw new Error(`invalid value of k : ${k}`);
	}
	this.k = k;
    }
    
    call(nodesSelection: d3NodesSelection, linksSelection: d3LinksSelection) {
	let nodes = nodesSelection.data();
	if (nodes.length < this.k) {
	    return;
	}

	let centers = nodes.slice(0, this.k).map((node) => {
	    return {x: node.x, y: node.y};
	});
	let oldClusters: Array<Set<Node>> = undefined;
	let clusters: Array<Set<Node>>;
	while (true) {
	    clusters = centers.map((center) => {return new Set()});
	    for (let node of nodes) {
		let minSquareDist = undefined;
		let chosenCenter = undefined;
		for (let i = 0; i < centers.length; i++) {
		    let center = centers[i];
		    let squareDist = (node.x - center.x) ** 2 + (node.y - center.y) ** 2
		    if (!minSquareDist || squareDist < minSquareDist) {
			minSquareDist = squareDist;
			chosenCenter = i;
		    }
		}
		clusters[chosenCenter].add(node);
	    }
	    if (!oldClusters || arrayEquals(clusters, oldClusters)) {
		centers = clusters.map((cluster) => {
		    let x_sum = 0;
		    let y_sum = 0;
		    cluster.forEach((node) => {
			x_sum += node.x;
			y_sum += node.y;
		    })
		    return {
			x: x_sum / cluster.size,
			y: y_sum / cluster.size
		    }
		});
		oldClusters = clusters;
	    } else {
		break;
	    }
	}

	for (let i = 0; i < clusters.length; i++) {
	    let cluster = clusters[i];
	    let clusterColor = colorsList[i % colorsList.length];
	    nodesSelection.selectAll('circle')
		.filter((node: Node) => {
		    return cluster.has(node);
		})
		.attr('fill', clusterColor);
	}
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	let kLabel = document.createElement("label");
	kLabel.textContent = "K";
	parent.appendChild(kLabel);

	let kInput = document.createElement("input") as HTMLInputElement;
	kInput.setAttribute("type", "text");
	kInput.value = this.k.toString();
	kInput.addEventListener("input", (event: any) => {
	    this.k = event.target.value; 
	    redrawCallback();
	});
	parent.appendChild(kInput);
    }
};

export const allLenseTypes = [ClusterKMeansLens, DegreeCentralityLens];
