import { Node, Link } from './graph'

export interface Filter {
    call: (nodes: Node[], links: Link[]) => {
	nodes: Node[],
	links: Link[],
    },
    buildUI: (parent: HTMLElement, redrawCallback: () => void) => void
};

export class MinInteractionsCountFilter implements Filter {

    interactionsCount: number;

    constructor(interactionsCount: number = 50) {
	this.interactionsCount = interactionsCount;
    }

    call(nodes: Node[], links: Link[]): {nodes: Node[], links: Link[]} {
	return {
	    nodes: nodes.filter((node) => {
		return node.interactionsCount >= this.interactionsCount;
	    }),
	    links: links.filter((link) => {
		return (
		    link.target.interactionsCount >= this.interactionsCount
			&& link.source.interactionsCount >= this.interactionsCount
		);
	    })
	};
    }

    buildUI(parent: HTMLElement, redrawCallback: () => void) {
	let interactionsCountLabel = document.createElement("label");
	interactionsCountLabel.textContent = "Interactions count";
	parent.appendChild(interactionsCountLabel);

	let interactionsCountInput = document.createElement("input") as HTMLInputElement;
	interactionsCountInput.setAttribute("type", "text");
	interactionsCountInput.value = this.interactionsCount.toString();
	interactionsCountInput.addEventListener("input", (event: any) => {
	    this.interactionsCount = event.target.value;
	    redrawCallback();
	});
	parent.appendChild(interactionsCountInput);
    }
};


export const allFilterTypes = [MinInteractionsCountFilter];
