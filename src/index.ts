import { app, BrowserWindow, Menu, dialog, ipcMain } from 'electron'
import * as path from 'path'
import * as fs from 'fs'
import $ = require('jquery')

const debug = require('electron-debug')
debug()

if (require('electron-squirrel-startup')) {
  app.quit()
}

export type Preferences = {
  darkTheme: boolean
  debugAnalysePath: string
}

const createWindow = () => {
  let preferences: Preferences = require('../default_preferences.json')
  if (fs.existsSync(path.join(__dirname, '../preferences.json'))) {
    let custom_preferences: Preferences = require('../preferences.json')
    preferences = { ...preferences, ...custom_preferences }
  }

  const mainWindow = new BrowserWindow({
    height: 900,
    width: 1600,
    webPreferences: {
      nodeIntegration: true,
    },
  })

  const openFile = (filepath: string) => {
    const analyse = require(filepath)
    // mainWindow.loadFile('src/visualization.html')
    // console.log('openFile')
    // $('#visualization').load('src/visualization.html')
    mainWindow.webContents.on('did-finish-load', () => {
      mainWindow.webContents.send('load', preferences)
      mainWindow.webContents.send('load-analyse', analyse)
    })
  }

  let menuTemplate = [
    {
      label: 'File...',
      submenu: [
        {
          label: 'Open...',
          accelerator: 'Ctrl+O',
          click() {
            dialog
              .showOpenDialog({
                properties: ['openFile'],
              })
              .then((dialogResult: Electron.OpenDialogReturnValue) => {
                if (dialogResult.canceled) {
                  return
                }
                let filepath = dialogResult.filePaths[0]
                openFile(filepath)
              })
              .catch()
          },
        },
        {
          label: 'Quit',
          accelerator: 'Ctrl+Q',
          click() {
            app.quit()
          },
        },
        {
          label: 'Debug',
          accelerator: 'Ctrl+D',
          click() {
            mainWindow.webContents.openDevTools()
            if (preferences.debugAnalysePath) {
              openFile(preferences.debugAnalysePath)
            }
          },
        },
      ],
    },
  ]
  ipcMain.on('set-contextual-menu', (event, contextualMenu: any[]) => {
    for (let menu of contextualMenu) {
      for (let submenu of menu.submenu) {
        submenu.click = () => {
          mainWindow.webContents.send(submenu.signal)
        }
      }
    }
    mainWindow.setMenu(Menu.buildFromTemplate(menuTemplate.concat(contextualMenu)))
  })
  let menu = Menu.buildFromTemplate(menuTemplate)
  mainWindow.setMenu(menu)

  mainWindow.loadFile(path.join(__dirname, '../src/index.html'))
  mainWindow.webContents.on('did-finish-load', () => {
    mainWindow.webContents.send('load', preferences)
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
